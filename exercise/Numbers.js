function verifyNumber(number)
{
  var divisorCount = 0;
  for (i = 1; i <= number; i++){
    if( number % i == 0){
      divisorCount++;
    }
  }

  if(divisorCount == 2)
  {
    return 'primo';
  }
  else if( number % 2 == 0){
    return 'par';
  }
  else {
    return 'compuesto';
  }

}
